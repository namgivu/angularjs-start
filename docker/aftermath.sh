#!/bin/bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`

    source "$SH/.config.sh"

        docker ps --format '{{.Names}} {{.Ports}} {{.Image}}' | grep -E "$CONTAINER_NAME|$PORT|$IMAGE_NAME" --color=always
        if [ $? != 0 ]; then
            echo "Container not found $CONTAINER_NAME"
            exit 1
        fi

            curl -Is localhost:$PORT | grep '200 OK'
            if [ $? != 0 ]; then
                echo "The webapp not serving at localhost:$PORT"
                exit 1
            fi

echo 'Everything looks good'
