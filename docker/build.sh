#!/bin/bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`
AH=`cd "$SH/.." && pwd`

    source "$SH/.config.sh"

        docker image rm -f $IMAGE_NAME

        docker build  --file "$SH/Dockerfile"  -t $IMAGE_NAME  $AH
        #             .                        image tag       app to build
