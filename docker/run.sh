#!/bin/bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`

    source "$SH/.config.sh"

    docker run  --name $CONTAINER_NAME  -p $PORT:4200  -d  $IMAGE_NAME
    #      .    container name            port         .   image tag

        echo; printf "Wait until the container ready ..."
        while true; do
            found_ready=`docker logs $CONTAINER_NAME | grep -c 'is listening on 0.0.0.0:4200'`
            if [ $found_ready = 1 ]; then
                break
            fi
            printf '.'; sleep 1
        done
        echo ' Done'
