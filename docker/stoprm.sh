#!/bin/bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`

    source "$SH/.config.sh"

        docker stop -t1 $CONTAINER_NAME
        docker rm   -f  $CONTAINER_NAME
